var mysqlQuery = require('../../mysql/query.js')
  , emptyDatabase = require('../support/empty_database.js');

describe('saveNewTask', function () {
  var fn = require('../../app/mysql/save_new_task.js');

  beforeEach(function (done) {
    emptyDatabase().then(done);
  });

  it('saves task to database', function (done) {
    var payload = {
      session: { userId: Math.ceil(Math.random() * 1000) },
      newTask: { text: Math.random().toString() }
    };
    var result;
    fn(payload)
      .then(function (_result) {
        result = _result;
        expect(result).toBe(payload);
        return mysqlQuery({ sql: 'SELECT * FROM tasks' });
      })
      .then(function (mysqlResult) {
        expect(mysqlResult.rows[0].text).toEqual(payload.newTask.text);
        expect(mysqlResult.rows[0].userId).toEqual(payload.session.userId);
        expect(result.task).toEqual({
          id: mysqlResult.rows[0].id,
          text: mysqlResult.rows[0].text
        });
        done();
      });
  });
});
