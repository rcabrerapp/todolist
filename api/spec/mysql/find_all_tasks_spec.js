var mysqlQuery = require('../../mysql/query.js'),
    emptyDatabase = require('../support/empty_database.js');

describe('findAllTasks', function () {
  var fn = require('../../app/mysql/find_all_tasks.js'),
      userId;

  beforeEach(function (done) {
    emptyDatabase()
      .then(function () {
        userId = Math.ceil(Math.random() * 1000);
        return mysqlQuery({
          sql: 'INSERT INTO tasks (text, userId) VALUES (?, ?), (?, ?), (?, ?)',
          values: ['task1', userId, 'task2', userId, 'task3', Math.ceil(Math.random() * 1000)]
        });
      })
      .then(done);
  });

  it ('find all the users tasks', function (done) {
    var payload = {
      session: { 
        id: Math.ceil(Math.random() * 1000),
        token: Math.random(),
        userId: userId 
      }
    }
    var result;
    fn(payload)
      .then(function (_result) {
        result = _result;
        expect(result).toBe(payload);
        return mysqlQuery({ 
          sql: 'SELECT * FROM tasks WHERE userId = ?',
          values: [payload.session.userId]
        });
      })
      .then(function (mysqlResult) {
        expect(result.tasks).toEqual([
          {
            id: mysqlResult.rows[0].id,
            text: mysqlResult.rows[0].text,
            completed: mysqlResult.rows[0].completed,
          },
          {
            id: mysqlResult.rows[1].id,
            text: mysqlResult.rows[1].text,
            completed: mysqlResult.rows[1].completed,
          }
        ])
        done();
      })
  });
})