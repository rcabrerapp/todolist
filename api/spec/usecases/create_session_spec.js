var emptyDatabase = require('../support/empty_database.js')
  , mysqlQuery = require('../../mysql/query.js');

describe('createSession', function () {
  var fn = require('../../app/usecases/create_session.js')
    , user;

  beforeEach(function (done) {
    emptyDatabase()
      .then(function () {
        user = { email: Math.random().toString() };
        return mysqlQuery({
          sql: 'INSERT INTO users (email) VALUES (?)',
          values: [user.email]
        });
      })
      .then(function (result) {
        user.id = result.rows.insertId;
      })
      .then(done);
  });

  it('finds user and creates new session', function (done) {
    var payload = { user: { email: user.email } }
      , result;
    fn(payload)
      .then(function (_result) {
        result = _result;
        return mysqlQuery({ sql: 'SELECT * FROM sessions' });
      })
      .then(function (mysqlResult) {
        expect(mysqlResult.rows[0].userId).toEqual(user.id);
        expect(result).toEqual({
          session: {
            token: mysqlResult.rows[0].token
          }
        });
        done();
      });
  });

  it('creates user and session', function (done) {
    var payload = { user: { email: Math.random().toString() } }
      , result;
    fn(payload)
      .then(function (_result) {
        result = _result;
        return mysqlQuery({
          sql: 'SELECT * FROM users WHERE email = ?',
          values: [payload.user.email]
        });
      })
      .then(function (mysqlResult) {
        expect(mysqlResult.rows[0].email).toEqual(payload.user.email);
        return mysqlQuery({
          sql: 'SELECT * FROM sessions WHERE userId = ?',
          values: mysqlResult.rows[0].id
        });
      })
      .then(function (mysqlResult) {
        expect(mysqlResult.rows[0]).not.toBeNull();
        expect(result).toEqual({
          session: {
            token: mysqlResult.rows[0].token
          }
        });
        done();
      });
  });
});
