var mysqlQuery = require('../../mysql/query.js')
  , emptyDatabase = require('../support/empty_database.js');

describe('addTask', function () {
  var fn = require('../../app/usecases/add_task.js')
    , session;

  beforeEach(function (done) {
    emptyDatabase()
      .then(function () {
        session = {
          token: Math.random(),
          userId: Math.ceil(Math.random() * 1000)
        };
        return mysqlQuery({
          sql: 'INSERT INTO sessions (token, userId) VALUES (?, ?)',
          values: [session.token, session.userId]
        });
      })
      .then(function (result) {
        session.id = result.rows.insertId;
      })
      .then(done);
  });

  it('session error', function (done) {
    var payload = { session: { token: Math.random() } };
    fn(payload).catch(function (error) {
      expect(error).toEqual({ session: 'NOT_FOUND' });
      done();
    });
  });

  it('validation error', function (done) {
    var payload = {
      session: { token: session.token },
      newTask: {}
    };
    fn(payload).catch(function (error) {
      expect(error).toEqual({ 'newTask.text': 'MISSING' });
      done();
    });
  });

  it('saves task', function (done) {
    var payload = {
      session: { token: session.token },
      newTask: { text: Math.random().toString() }
    };
    var result;
    fn(payload)
      .then(function (_result) {
        result = _result;
        return mysqlQuery({ sql: 'SELECT * FROM tasks' });
      })
      .then(function (mysqlResult) {
        expect(mysqlResult.rows[0].text).toEqual(payload.newTask.text);
        expect(mysqlResult.rows[0].userId).toEqual(session.userId);
        expect(result).toEqual({
          task: {
            id: mysqlResult.rows[0].id,
            text: mysqlResult.rows[0].text,
            // BUG: completed missing
          }
        });
        done();
      });
  });
});
