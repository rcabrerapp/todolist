var mysqlQuery = require('../../mysql/query.js')
  , emptyDatabase = require('../support/empty_database.js');

describe('GetAllTasks', function () {
  var fn = require('../../app/usecases/get_all_tasks.js'),
      session;

  beforeEach(function (done) {
    emptyDatabase()
      .then(function () {
        session = {
          token: Math.random(),
          userId: Math.ceil(Math.random() * 1000)
        };
        return mysqlQuery({
          sql: 'INSERT INTO sessions (token, userId) VALUES (?, ?)',
          values: [session.token, session.userId]
        });
      })
      .then(function (result) {
        session.id = result.rows.insertId;
      })
      .then(function () {
        return mysqlQuery({
          sql: 'INSERT INTO tasks (text, userId) VALUES (?, ?), (?, ?), (?, ?)',
          values: ['task1', session.userId, 'task2', session.userId, 'task3', Math.ceil(Math.random() * 1000)]
        });
      })
      .then(done);
  });

  it('session error', function (done) {
    var payload = { session: { token: Math.random() } };
    fn(payload).catch(function (error) {
      expect(error).toEqual({ session: 'NOT_FOUND' });
      done();
    });
  });

  it('get all tasks', function (done) {
    var payload = { session: { token: session.token } };
    var result;
    fn(payload)
      .then(function (_result) {
        result = _result
        return mysqlQuery({
          sql: 'SELECT * FROM tasks WHERE userId = ?',
          values: [session.userId]
        })
      })
      .then(function (mysqlResult) {
        expect(result.tasks).toEqual([
          {
            id: mysqlResult.rows[0].id,
            text: mysqlResult.rows[0].text,
            completed: mysqlResult.rows[0].completed,
          },
          {
            id: mysqlResult.rows[1].id,
            text: mysqlResult.rows[1].text,
            completed: mysqlResult.rows[1].completed,
          }
        ])
        done();
      });
  });
});
