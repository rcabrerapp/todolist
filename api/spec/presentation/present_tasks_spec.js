describe('presentTask', function () {
  var fn = require('../../app/presentation/present_tasks.js');

  it('presents tasks', function (done) {
    var payload = {
      session: { 
        id: Math.ceil(Math.random() * 1000),
        token: Math.random(),
        userId: Math.ceil(Math.random() * 1000) 
      },
      tasks: [
        {
          id: Math.random(),
          text: Math.random(),
          completed: false
        },
        {
          id: Math.random(),
          text: Math.random(),
          completed: false
        },
        {
          id: Math.random(),
          text: Math.random(),
          completed: true
        }
      ]
    };
    fn(payload).then(function (result) {
      expect(result).toEqual({
        tasks: [
          {
            id: payload.tasks[0].id,
            text: payload.tasks[0].text,
            completed: payload.tasks[0].completed
          },
          {
            id: payload.tasks[1].id,
            text: payload.tasks[1].text,
            completed: payload.tasks[1].completed
          },
          {
            id: payload.tasks[2].id,
            text: payload.tasks[2].text,
            completed: payload.tasks[2].completed
          }
        ]
      });
      done();
    })
  })
});
