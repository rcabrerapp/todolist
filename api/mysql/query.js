var mysql = require('mysql');

function createPool() {
  return mysql.createPool({
    connectionLimit: 10,
    host:     process.env.MYSQL_HOST || 'localhost',
    user:     process.env.MYSQL_USER || 'root',
    password: process.env.MYSQL_PASS || 'root',
    database: process.env.MYSQL_NAME || 'todolist'
  });
}

global.MYSQL_POOL = global.MYSQL_POOL || createPool();

module.exports = function mysqlQuery(query) {
  return new Promise(function (resolve, reject) {
    var queryConfig = {
      sql: query.sql,
      timeout: 10000,
      values: query.values
    };
    global
      .MYSQL_POOL
      .query(queryConfig, function (error, rows, fields) {
        if (error)
          reject(error);
        else
          resolve({ rows: rows, fields: fields });
      });
  });
};

