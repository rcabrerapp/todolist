var mysqlQuery = require('../../mysql/query.js');

module.exports = function findSession(payload) {
  return mysqlQuery({
    sql: 'SELECT * FROM sessions WHERE token = ?',
    values: [payload.session.token]
  })
    .then(function (mysqlResult) {
      if (!mysqlResult.rows[0])
        throw { session: 'NOT_FOUND' };
      Object.assign(payload.session, {
        id: mysqlResult.rows[0].id,
        userId: mysqlResult.rows[0].userId,
      });
      return payload;
    });
};
