var mysqlQuery = require('../../mysql/query.js');

module.exports = function findAllTasks(payload) {
  return mysqlQuery({
    sql: 'SELECT * FROM tasks WHERE userId = ?',
    values: [payload.session.userId]
  })
    .then(function (mysqlResult) {
      payload.tasks = []
      mysqlResult.rows.forEach(row => {
        payload.tasks.push({
          id: row.id,
          text: row.text,
          completed: row.completed
        });
      });
      return payload;
    })
};
