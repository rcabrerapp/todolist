var findSession = require('../mysql/find_session.js')
  , validateNewTask = require('../validation/validate_new_task.js')
  , saveNewTask = require('../mysql/save_new_task.js')
  , presentTask = require('../presentation/present_task.js');

module.exports = function addTask(payload) {
  return Promise.resolve(payload)
    .then(findSession)
    .then(validateNewTask)
    .then(saveNewTask)
    .then(presentTask);
};
