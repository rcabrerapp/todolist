var findSession = require('../mysql/find_session'),
    findAllTasks = require('../mysql/find_all_tasks'),
    presentTasks = require('../presentation/present_tasks');

module.exports = function getAllTasks(payload) {
  return Promise.resolve(payload)
    .then(findSession)
    .then(findAllTasks)
    .then(presentTasks);
};
