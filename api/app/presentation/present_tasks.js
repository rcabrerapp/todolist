module.exports = function presentTask(payload) {
  return Promise.resolve({
    tasks: payload.tasks
  }
  );
};