var http = require('http')
  , path = require('path')
  , fs = require('fs');

var routeRequest = require('./router/route_request.js');

var server = http.createServer();

var socket = path.join(__dirname, './index.socket');

server.on('request', routeRequest);

try { fs.unlinkSync(socket); }
catch (e) { }

server.listen(socket);
